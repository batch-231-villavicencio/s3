// To create a class in JS, we use the class keyword and {}
// naming convention for classes begins with uppercase characters
/*
    Syntax:
    class ClassName {
        properties
    };
*/

class Dog {
    // we add constructor method to a class to be able to initialize values upon instantiation of an object from a class
    constructor(name, breed, age){
        this.name = name;
        this.breed = breed;
        this.age = age*7;
    }
}

// instantiation - process of creating objects 
// an object created class is called an instance
// "new" keyword is used to instantiate an object from a class

let dog1 = new Dog("Puchi", "golden retriever", 0.6);
console.log(dog1);

class Person {
    constructor(name,age,nationality,address){
        this.name = name;
        this.nationality = nationality;
        this.address = address;
        if (typeof age === "number" && age > 17) {
            this.age = age;
        } else {
            this.age = undefined
        }
    }
    greet(){
        console.log(`Hello! Good Morning!`)
        return this
    }
    introduce(){
        console.log(`Hi! My name is ${this.name}`)
        return this
    }
    changeAddress(newAddress){
        this.address = newAddress
        console.log(`${this.name} now lives in ${this.address}`)
        return this
    }

}

let person1 = new Person("John Smith",30,"American","New York City");
let person2 = new Person("Juan Dela Cruz",28,"Filipino","Angeles City")

/*
    1. What is the blueprint where objects are created from?
        Answer: class


    2. What is the naming convention applied to classes?
        Answer: pascal case


    3. What keyword do we use to create objects from a class?
        Answer: new


    4. What is the technical term for creating an object from a class?
        Answer: instantiation


    5. What class method dictates HOW objects will be created from that class?
        Answer: constructor

*/

// class student

class Student {
    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        //check first if the array has 4 elements
        if(grades.length === 4){
            if(grades.every(grade => grade >= 0 && grade <= 100)){
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;

    }
    // class methods
    login(){
        console.log(`${this.email} has logged in`);
        return this;
    }
    logout(){
        console.log(`${this.email} has logged out`)
        return this;
    }
    listGrades(){
        this.grades.forEach(grade => {
            console.log(`${this.name}'s quarterly grade averages are: ${grade}`)
        })
        return this;
    }
    computeAve(){
        let sum = 0
        this.grades.forEach(grade => sum = sum+grade)
        this.gradeAve = sum / 4;
        return this;
    }
    willPass(){
        this.passed = this.computeAve().gradeAve >= 85 ? true : false
        return this;

    }
    willPassWithHonors(){
        if(this.passed){
            if(this.gradeAve >= 90){
                this.passedWithHonors =true;
            } else {
                this.passedWithHonors =false;
            }
        } else {
            this.passedWithHonors =false;
        }
        return this;
    }
}

// instantiate all four students from s2 using student class

let studentOne = new Student("Tony","starksindustries@mail.com",[89, 84, 78, 88]);
let studentTwo = new Student("Peter","spideyman@mail.com",[78, 82, 79, 85]);
let studentThree = new Student("Wanda","scarlettMaximoff@mail.com",[87, 89, 91, 93]);
let studentFour = new Student("Steve","captainRogers@mail.com",[91, 89, 92, 93]);

/*

    1. Yes
    2. No
    3. Yes
    4. dot notation
    5. object / return.this

*/

